﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 pos;

    Rigidbody rb;
    float moveSpeed,jumpForce;
    float turnSmoothTime = 0.1f;
    float turnSmoothVelocity ;
    public Transform cam;
    bool grounded;

    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = 20f;
        jumpForce = 10f;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputZ = Input.GetAxis("Vertical");
        pos = new Vector3(inputX * moveSpeed, 0, inputZ * moveSpeed);
        
        

    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(new Vector3(0,5) ,ForceMode.Impulse);
            grounded = false;
        }
        if (pos.magnitude >= 0.1f)
        {
            Debug.Log(pos.magnitude);
            float targetAngle = Mathf.Atan2(pos.x, pos.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Vector3 moveDir = Quaternion.Euler(0f, angle, 0) * Vector3.forward;
            transform.position += moveDir;//FIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            grounded = true;
        }
    }
}
